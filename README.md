# GitLab MR Approve Notification Bot

This bot sends Slack DM to GitLab MR approvers when MR is opened.

## Requirements

- GitLab token with api permission
- Slack API token which can access these scopes;
    - chat:write
    - users.profile:read
    - users:read
    - users:read.email

## Environment Variables

- SLACK_TOKEN
- GITLAB_TOKEN
- GITLAB_URL (https://gitlab.example.com)
- MESSAGE_TEMPLATE : The message to be sent to approvers

## Installation

This application listens port 3000 for incoming HTTP requests.
 There is only one endpoint(/) for accepting POST requests.
 It can be run with the following commands;

 `yarn install && node index.js`

After application is started you need to add a new webhook for your GitLab instance or Repository.

## Security

This application does not have any security measures.
 So you should install it on your GitLab instance 
 and enable “Allow requests to the local network from system hooks” in the “Outbound requests” section inside the Admin Area > Settings (/admin/application_settings/network)

## Licence

mr-notification is available under the MIT license.