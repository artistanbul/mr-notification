const express = require('express');
const app = express();
const axios = require('axios')
const port = 3000;
const { WebClient } = require('@slack/web-api');

app.use(express.json())

const slackToken = process.env.SLACK_TOKEN;
const slackAPI = new WebClient(slackToken);

const ax = axios.create({
  baseURL: process.env.GITLAB_URL,
  headers: {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + process.env.GITLAB_TOKEN
  },
})

app.post('/', (req, res) => {
  const action = req.body.object_attributes.action;
  if (action !== "open") {
    res.status(400).end();
    return;
  }

  const project_id = req.body.object_attributes.target_project_id;
  const mr_iid = req.body.object_attributes.iid;
  const url = req.body.object_attributes.url;

  ax.get(`/api/v4/projects/${project_id}/merge_requests/${mr_iid}/approvals`)
    .then(function (response) {
      let data = response.data
      if (data.approvals_required > 0 && !data.approved) {
        for (let i = 0; i < data.suggested_approvers.length; i++) {
          const approver = data.suggested_approvers[i];
          getUserEmail(approver.id).then((email) => {
            getUserSlackID(email).then((id) => {
              notifyUser(id, url)
              res.status(200).end();
            })
          })
        }
      }
    })
    .catch(function (error) {
      console.log(error);
      res.status(400).end();
    })

});

function notifyUser(id, url) {
  const text = `${process.env.MESSAGE_TEMPLATE}\n${url}`
  slackAPI.chat.postMessage({ channel: id, text: text })
    .catch((error) => {
      console.log(error);
    })
}

function getUserEmail(userID) {
  return ax.get(`/api/v4/users/${userID}`)
    .then(response => response.data.email)
    .catch(function (error) {
      console.log(error);
    })
}

function getUserSlackID(email) {
  return slackAPI.users.lookupByEmail({ email: email })
    .then(response => response.user.id)
    .catch(function (error) {
      console.log(error);
    })
}

app.listen(port, () => {
  console.log(`mr-notification app listening on port ${port}!`)
});